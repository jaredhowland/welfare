<?php
/**
  * Submit a new order
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-04-28
  * @since 2014-03-12
  *
  */

require_once 'config.php';

$submit = new submit;
$html = $submit->form($_POST);

template::display('generic.tmpl', array('html' => array('html' => $html)));

?>
