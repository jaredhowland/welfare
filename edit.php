<?php
/**
  * Home page
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-03-13
  * @since 2014-03-13
  *
  */

require_once 'config.php';

$id = (int) $_GET['id'];

$edit = new edit;
$html = $edit->html($id);

template::display('forms.tmpl', $html);

?>
