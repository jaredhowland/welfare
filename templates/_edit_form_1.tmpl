{% set app_url = constant('config::URL') %}
{% spaceless %}
{% autoescape false %}

{% macro start(item, item_id, header) %}
  {% set header = header|default(false) %}
  {% set id = item|lower|replace({' ':'_', ',':'_'}) %}
  {% if header %}
    <table id="table_{{ header }}">
      <thead>
        <tr><th>Item</th><th>Unit Cost</th></tr>
      </thead>
      <tbody>
      <tr><th colspan="2" id="{{ item_id }}">{{ item }}</th></tr>
  {% else %}
    <tr><th colspan="2" id="{{ item_id }}">{{ item }}</th></tr>
  {% endif %}
{% endmacro %}

{% macro end() %}
      </tbody>
    </table>
  </td>
  <td>
{% endmacro %}

{% macro row(item_id, item, unit_cost) %}
  <tr><td id="A{{ item_id }}"><a href="#" class="edit" id="item_{{ item_id }}">{{ item }}</a></td><td id="B{{ item_id }}" data-format="$ 0.00"><a href="#" class="edit" id="unitcost_{{ item_id }}">{{ unit_cost|number_format(2, '.', ',') }}</a></td></tr>
{% endmacro %}

{# Import macros for use in this template #}
{% import _self as table %}

<h1>Edit Form</h1>
<p><span class="label label-danger">WARNING</span> Editing this data will change <strong>all</strong> orders that have been entered with this form. For example, if you edit the unit cost, it will change the grand totals for all previous orders that used this form. If you would rather create a brand new form, which would avoid this problem, please <a href="https://bitbucket.org/jaredhowland/welfare/issues/new">submit a ticket</a> describing the request in detail.</p>
<p><span class="label label-success">TIPS</span> Click any item you would like to edit. After changing the data, press enter to record the change. Changes are saved automatically after pressing enter.</p>

<table id="container_table">
  <tr><td>

{% set current_column = 0 %}
{% set current_section = 0 %}
{% set first_row_id = null %}
{% set last_row_id = null %}

{% for result in results %}
  {% if result.column_number == current_column %}
    {% if result.section_id == current_section %}
    {# If it is a regular row (not a new subhead or a new column) #}
      {{ table.row(result.item_id, result.item, result.unit_cost, result.quantity) }}
      {% set last_row_id = result.item_id %}
    {% else %}
    {# If it is the beginning of a new subsection #}
      {{ table.start(result.section_name, result.item_id) }}
      {{ table.row(result.item_id, result.item, result.unit_cost, result.quantity) }}
      {% set current_section = current_section + 1 %}
    {% endif %}
  {% else %}
  {# If it is the beginning of a new column #}
    {% if result.column_number != 1 %}
    {# If it is the end of a new column (excluding the grand total table which we do not want to close) #}
      {% set last_row_id = result.item_id - 1 %}
      <tr class="subtotal"><th></th><th></th></tr>
      {% set subtotal_id = subtotal_id + 1 %}
      {{ table.end() }}
    {% endif %}
    {# Start new table #}
    {{ table.start(result.section_name, result.item_id, result.column_number) }}
    {{ table.row(result.item_id, result.item, result.unit_cost, result.quantity) }}
    {% set first_row_id = result.item_id %}
    {% set current_column = current_column + 1 %}
    {% set current_section = current_section + 1 %}
  {% endif %}
{% endfor %}

          <tr class="subtotal"><th></th><th></th></tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>

{% endautoescape %}
{% endspaceless %}
