<?php
/**
  * Past orders page
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-04-29
  * @since 2014-04-29
  *
  */

require_once 'config.php';

$past_orders = new past_orders;
$order_data  = $past_orders->view();

template::display('past_orders.tmpl', array('title' => 'Past Orders', 'recipients' => $order_data['recipients'], 'chart' => $order_data['chart']));

?>
