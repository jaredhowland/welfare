<?php
/**
  * Configuration class.
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-05-01
  *
  */

class config {
  // Database settings
  const DB_HOST     = '';
  const DB_PORT     = '';
  const DB_NAME     = '';
  const DB_USERNAME = '';
  const DB_PASSWORD = '';

  // APP SETTINGS
  const DEVELOPMENT = TRUE; // Changes the app behavior (error reporting, template caching, etc.)
  const URL         = 'http://full.url.to/directory';
  const TIME_ZONE   = 'America/Denver';

/****************************************************************************/
/*                       DO NOT EDIT BELOW THIS LINE                        */
/****************************************************************************/

  /**
    * Determines type of error reporting
    * Based on state of DEVELOPMENT constant
    *
    * @param null
    * @return string Type of error reporting
    */
  public static function set_error_reporting() {
    if(self::DEVELOPMENT) {
      ini_set('error_reporting', E_ALL^E_NOTICE);
      ini_set('display_errors', 1);
    } else {
      error_reporting(0);
    }
  }

} // End class


/****************************************/
/* Miscellaneous configuration settings */
/****************************************/

// Autoload classes
// Must be in the 'classes' directory and prefixed with 'class.'
function __autoload($class) {
  require_once(__DIR__ . '/classes/class.' . $class . '.php');
}

// Set default time zone
date_default_timezone_set(config::TIME_ZONE);

// Set error reporting
config::set_error_reporting();

?>
