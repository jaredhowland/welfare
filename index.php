<?php
/**
  * Home page
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-03-11
  * @since 2014-03-11
  *
  */

require_once 'config.php';

$order = new order;
$html  = $order->html('new');

template::display('forms.tmpl', $html);

?>
