<?php
/**
  * Delete order
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-04-30
  * @since 2014-04-26
  *
  */

require_once 'config.php';

$id     = (int) $_GET['id'];
$delete = $_GET['delete'];

// Confirm user really wants to delete
if($id && !$delete) {
  $html = <<<HTML
    <p>Are you sure you want to delete this order?</p>
    <p class="delete"><a href="delete.php?id=$id&amp;delete=true" class="label label-success">Yes, delete this order</a></p>
    <p class="delete"><a href="edit.php?id=$id" class="label label-danger">Cancel</a></p>
HTML;
  $html = array('html' => array('html' => $html));
  template::display('generic.tmpl', $html);

// If they do want to delete an order, go ahead and delete it
} else {
  $delete = new delete;
  $html = $delete->delete_order($id);
  $html = array('html' => array('html' => $html));
  template::display('generic.tmpl', $html);
}

?>

