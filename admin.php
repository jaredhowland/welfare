<?php
/**
  * Admin page
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-03
  * @since 2014-05-03
  *
  */

require_once 'config.php';

$order = new order;
$html  = $order->html('new');

template::display('edit_form.tmpl', array('title' => 'Admin', 'results' => $html['results']));

?>
