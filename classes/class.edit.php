<?php
/**
  * Class to edit order already in database
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-03-13
  */

class edit extends query {
  /**
  * Returns array of data to be displayed to the user when editing an order
  *
  * @access public
  * @param int Order ID to edit
  * @return array HTML to put into template
  */
  public function html($id) {
    $items       = $this->get_items($id);
    $order_info  = $this->get_order_information($id);
    $subtotal_id = parent::next_item_id();
    $recipients  = parent::all_recipients();
    return array('title' => 'Edit Order', 'recipients' => $recipients, 'results' => $items, 'subtotal_id' => $subtotal_id, 'recipient' => $order_info['recipient'], 'order_date' => $order_info['order_date'], 'order_id' => $id);
  }

  /**
  * Returns array of all items to edit
  *
  * @access private
  * @param int order_id to edit
  * @return array Array with data about resources
  */
  private function get_items($order_id) {
    $form_id = parent::form_id($order_id);
    $db = new database;
    $db->query('SELECT fi.item_id, i.section_id, s.name AS section_name, s.column_number, i.item, i.unit_cost, oi.quantity FROM forms_items fi LEFT JOIN items i ON fi.item_id = i.id LEFT JOIN sections s ON i.section_id = s.id LEFT JOIN orders_items oi ON i.id = oi.item_id WHERE fi.form_id = :form_id AND oi.order_id = :order_id');
    $db->bindMore(array(':order_id' => $order_id, ':form_id' => $form_id));
    return $db->resultset();
  }

  /**
  * Returns array of formatted order date and recipient
  *
  * @access private
  * @param int order_id to edit
  * @return array Array with order date and recipient name
  */
  private function get_order_information($order_id) {
    $db = new database;
    $db->query('SELECT DATE_FORMAT(o.order_date, "%e %M %Y") AS order_date, CONCAT(r.first_name, " ", r.last_name) AS recipient FROM orders o LEFT JOIN recipients r ON o.recipient_id = r.id WHERE o.id = :order_id');
    $db->bind(':order_id', $order_id);
    return $db->single();
  }
}

?>
