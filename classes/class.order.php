<?php
/**
  * Class to display information from database
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2013-03-13
  */

class order extends query {
  /**
  * Returns array of data about welfare
  *
  * @access public
  * @param null
  * @return array Data for welfare form
  */
  public function html() {
    $items       = $this->get_items();
    $subtotal_id = parent::next_item_id();
    $recipients  = parent::all_recipients();
    return array('title' => 'New Order', 'results' => $items, 'subtotal_id' => $subtotal_id, 'recipients' => $recipients);
  }

  /**
  * Gets all items for active form
  *
  * @access private
  * @param null
  * @return array Array with data for active welfare form
  */
  private function get_items() {
    $form_id = parent::active_form_id();
    $db = new database;
    $db->query('SELECT fi.item_id, i.section_id, s.name AS section_name, s.column_number, i.item, i.unit_cost FROM forms_items fi LEFT JOIN items i ON fi.item_id = i.id LEFT JOIN sections s ON i.section_id = s.id WHERE fi.form_id = :form_id');
    $db->bind(':form_id', $form_id);
    return $db->resultset();
  }
}

?>
