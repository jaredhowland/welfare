<?php
/**
  * Class to update an existing order
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-04-29
  */

class update {

  private $submitted_data;

  /**
  * Updates data in database for an existing order
  *
  * @access public
  * @param array Data for updated order
  * @return array HTML to put into template
  */
  public function order($submitted_data) {
    $this->submitted_data = $submitted_data;
    $order_id = $submitted_data['order_id'];
    if($order_id == '') {
      $html = 'Please go back and select an order to update.';
      template::display('generic.tmpl', array('html' => array('html' => $html)));
      die();
    } else {
      return $this->update_order();
    }
  }

  /**
  * Updates an existing order
  *
  * @access private
  * @param null
  * @return string User message for successful update
  */
  private function update_order() {
    if($this->update_order_data() && $this->update_orders_items()) {
      return 'The order information was successfully updated.';
    } else {
      $html = 'There was a problem submitting this order. Please go back and try again.';
      template::display('generic.tmpl', array('html' => array('html' => $html)));
      die();
    }
  }

  /**
  * Updates orders table
  *
  * @access private
  * @param null
  * @return bool TRUE on success, FALSE on failure
  */
  private function update_order_data() {
    $order_id   = $this->submitted_data['order_id'];
    $order_date = date('Y-m-d', strtotime($this->submitted_data['order_date']));
    $db = new database;
    $db->query('UPDATE orders SET order_date = :order_date WHERE id = :order_id');
    $db->bindMore(array(':order_id' => $order_id, ':order_date' => $order_date));
    return $db->execute();
  }

  /**
  * Updates orders_items table
  *
  * @access private
  * @param null
  * @return bool TRUE on success, FALSE on failure
  */
  private function update_orders_items() {
    // Remove the last 4 data points from submitted data and store in variables
    // Variables are not needed right now but we may as well store them in case we need
    // them in the future
    $order_id   = array_shift($this->submitted_data);
    $order_date = array_pop($this->submitted_data);
    $db = new database;
    $db->beginTransaction();
    foreach($this->submitted_data AS $item_id => $quantity) {
      $item_id = ltrim($item_id, 'C');
      $db->query('UPDATE orders_items SET quantity = :quantity WHERE item_id = :item_id AND order_id = :order_id');
      $db->bindMore(array(':item_id' => $item_id, ':order_id' => $order_id, ':quantity' => $quantity));
      if($db->execute() === false) {
        $db->cancelTransaction();
        return false;
      }
    }
    $db->endTransaction();
    return true;
  }
}

?>
