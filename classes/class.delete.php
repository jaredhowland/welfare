<?php
/**
  * Class to display information from database
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-04-26
  */

class delete {
    /**
    * Attempts to delete the order and returns user message
    *
    * @access public
    * @param int order_id
    * @return string User message on success or failure of delete
    */
    public function order($id) {
      if($this->delete_order($id)) {
        return 'Order successfully deleted.';
      } else {
        return 'The order was not deleted. Please try again.';
      }
    }

    /**
    * Deletes order
    *
    * @access private
    * @param int Order ID
    * @return bool TRUE if success, FALSE otherwise
    */
    private function delete_order($id) {
      $db = new database;
      $db->query('DELETE FROM orders WHERE id = :id');
      $db->bind(':id', $id);
      return $db->resultset();
    }

}

?>
