<?php
/**
  * Class to submit a new order
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-04-28
  */

class submit extends query {

  private $submitted_data;

  /**
  * Inserts data into database for a new order
  *
  * @access public
  * @param array Data inserted into form and submitted
  * @return array HTML to put into template
  */
  public function form($submitted_data) {
    $this->submitted_data = $submitted_data;
    $this->check_for_recipient();
    return $this->record_order();
  }

  /**
  * Checks to see if any recipient was entered
  * If not, it presents a message asking for a recipient
  * Otherwise, it returns null
  *
  * @access private
  * @param null
  * @return mixed string Message asking for a recipient
  *               null Nothing returned if a recipient is submitted
  */
  private function check_for_recipient() {
    $recipient  = $this->submitted_data['recipient'];
    $first_name = $this->submitted_data['first_name'];
    $last_name  = $this->submitted_data['last_name'];
    // If no old or new recipient is submitted with the order, display error message to user
    if($recipient == '' AND $first_name == '' AND $last_name == '') {
      $html = 'Please go back and select a recipient.';
      template::display('generic.tmpl', array('html' => array('html' => $html)));
      die();
    } else {
      return null;
    }
  }

  /**
  * Submits the order if a recipient exists or creates
  * recipient and then submits the order if they do not
  * already exist
  *
  * @access private
  * @param null
  * @return string Message telling you the order was submitted
  */
  private function record_order() {
    $recipient = $this->submitted_data['recipient'];
    if($recipient != '') {
      return $this->submit_order($recipient);
    } else {
      return $this->create_recipient();
    }
  }

  /**
  * Create recipient record
  *
  * @access private
  * @param null
  * @return null Passes the new recipient_id to the submit_order method
  */
  private function create_recipient() {
    $first_name = $this->submitted_data['first_name'];
    $last_name  = $this->submitted_data['last_name'];
    $db = new database;
    $db->query('INSERT INTO recipients (first_name, last_name) VALUES (:first_name, :last_name)');
    $db->bindMore(array(':first_name' => $first_name, ':last_name' => $last_name));
    $db->execute();
    return $this->submit_order($db->lastInsertId());
  }

  /**
  * Inserts data into the orders table
  *
  * @access private
  * @param int recipient_id
  * @return mixed int order_id of the newly created order
  *               bool FALSE if error
  */
  private function insert_order($recipient_id) {
    $form_id    = parent::active_form_id();
    $order_date = date('Y-m-d', strtotime($this->submitted_data['order_date']));
    $db = new database;
    $db->beginTransaction();
    $db->query('INSERT INTO orders (form_id, recipient_id, order_date) VALUES (:form_id, :recipient_id, :order_date)');
    $db->bindMore(array(':form_id' => $form_id, ':recipient_id' => $recipient_id, ':order_date' => $order_date));
    if($db->execute()) {
      $lastInsertId = (int) $db->lastInsertId();
      $db->endTransaction();
      return $lastInsertId;
    } else {
      $db->cancelTransaction();
      return false;
    }
  }

  /**
  * Inserts order into orders_items table
  *
  * @access private
  * @param int order_id
  * @return bool TRUE if successful, FALSE otherwise
  */
  private function insert_orders_items($order_id) {
    // Remove the last 4 data points from submitted data and store in variables
    // Variables are not needed right now but we may as well store them in case we need
    // them in the future
    $last_name    = array_pop($this->submitted_data);
    $first_name   = array_pop($this->submitted_data);
    $recipient_id = array_pop($this->submitted_data);
    $order_date   = array_pop($this->submitted_data);
    $db = new database;
    $db->beginTransaction();
    foreach($this->submitted_data as $item_id => $quantity) {
      $item_id = ltrim($item_id, 'C');
      $db->query('INSERT INTO orders_items (order_id, item_id, quantity) VALUES (:order_id, :item_id, :quantity)');
      $db->bindMore(array(':order_id' => $order_id, ':item_id' => $item_id, ':quantity' => $quantity));
      if($db->execute() === false) {
        $db->cancelTransaction();
        return false;
      }
    }
    $db->endTransaction();
    return true;
  }

  /**
  * Submits the order after a recipient_id is confirmed with previous method
  *
  * @access private
  * @param int recipient_id
  * @return string Message confirming data was entered
  */
  private function submit_order($recipient_id) {
    $order_id = $this->insert_order($recipient_id);
    $orders_items = $this->insert_orders_items($order_id);
    if($order_id AND $orders_items) {
      return 'The welfare food order was successfully recorded.';
    } else {
      return 'There was a problem entering this order. Please go back and try again.';
    }
  }
}

?>
