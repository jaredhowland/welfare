<?php
/**
  * Class to view individual recipient information about past orders
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-04-29
  */

class summary extends query {
  /**
  * Returns array for use in template to view individual recipient's past orders
  *
  * @access public
  * @param null
  * @return array Data to be used in template
  */
  public function view($recipient_id) {
    $all_orders_rows      = $this->get_all_orders_rows($this->get_all_orders($recipient_id));
    $average_by_year_rows = $this->get_average_by_year_rows($this->get_average_by_year($recipient_id));
    $list_all_orders_rows = $this->get_list_all_orders_rows($this->get_all_orders($recipient_id));
    $recipient            = $this->get_recipient_name($recipient_id);
    return array('title' => 'Individual Summary', 'recipient' => $recipient, 'all_orders_rows' => $all_orders_rows, 'average_by_year_rows' => $average_by_year_rows, 'list_all_orders_rows' => $list_all_orders_rows);
  }

  /**
  * Gets all orders in the system for a recipient
  *
  * @access private
  * @param int recipient_id
  * @return array All orders in the system
  */
  private function get_all_orders($recipient_id) {
    $db = new database(array(PDO::ATTR_PERSISTENT => true));
    $db->query('SELECT o.id, o.order_date, SUM(oi.quantity * i.unit_cost) AS grand_total FROM orders o LEFT JOIN orders_items oi ON o.id = oi.order_id LEFT JOIN items i ON oi.item_id = i.id WHERE o.recipient_id = :recipient_id GROUP BY o.id ORDER BY o.order_date DESC');
    $db->bind(':recipient_id', $recipient_id);
    return $db->resultset();
  }

  /**
  * All orders for a recipient in the system formatted for
  * Google Visualization API javascript to parse
  *
  * @access private
  * @param array All orders in the system
  * @return string All orders formatted for Google Visualization API
  */
  private function get_all_orders_rows($all_orders) {
    $chart = null;
    foreach($all_orders AS $order) {
      $order_id   = $order['id'];
      $order_date = date('Y-m-d', strtotime($order['order_date']));
      $year       = date('Y',     strtotime($order_date));
      $month      = date('n',     strtotime($order_date)) - 1;
      $day        = date('j',     strtotime($order_date));
      $total      = $order['grand_total'];
      $chart     .= '[new Date(' . $year . ', ' . $month . ', ' . $day . '),  ' . $total . '],';
      $html      .= '<p><strong><a href="edit.php?id=' . $order_id . '">' . $order_date . '</a></strong>: $' . $total . '</p>';
    }
    return rtrim($chart, ',');
  }

  /**
  * Average cost by year for a recipient
  *
  * @access private
  * @param int recipient_id
  * @return array Average cost by year for a recipient data
  */
  private function get_average_by_year($recipient_id) {
    $db = new database;
    $db->query('SELECT YEAR(o.order_date) AS year, SUM(oi.quantity * i.unit_cost) AS total_cost, COUNT(DISTINCT(o.id)) AS num_orders, ROUND((SUM(oi.quantity * i.unit_cost) / COUNT(DISTINCT(o.id))), 2) AS average FROM orders o LEFT JOIN orders_items oi ON o.id = oi.order_id LEFT JOIN items i ON oi.item_id = i.id WHERE o.recipient_id = :recipient_id GROUP BY year DESC WITH ROLLUP');
    $db->bind(':recipient_id', $recipient_id);
    return $db->resultset();
  }

  /**
  * Average by year for a recipient formatted for
  * Google Visualization API javascript to parse
  *
  * @access private
  * @param array Average by year for a recipient
  * @return string Average by year for a recipient data formatted for Google Visualization API
  */
  private function get_average_by_year_rows($averages_by_year) {
    $averages_by_year_rows = null;
    foreach($averages_by_year AS $average_for_year) {
      if(is_null($average_for_year['year'])) {
        $year = '\'OVERALL\'';
      } else {
        $year = '\'' . $average_for_year['year'] . '\'';
      }
      $averages_by_year_rows .= '[' . $year . ', ' . $average_for_year['total_cost'] . ', ' . $average_for_year['num_orders'] . ', ' . $average_for_year['average'] . '],';
    }
    return rtrim($averages_by_year_rows, ',');
  }

  /**
  * Formats all orders for use by
  * Google Visualization API javascript
  *
  * @access private
  * @param array All orders for a recipient
  * @return string All orders for a recipient formatted for Google Visualization API
  */
  private function get_list_all_orders_rows($orders) {
    $list_all_orders_rows = null;
    foreach($orders AS $order) {
      $order_id        = $order['id'];
      $formatted_date  = '<a href="edit.php?id=' . $order_id . '">' . date('j M Y', strtotime($order['order_date'])) . '</a>';
      $year            = date('Y', strtotime($order['order_date']));
      $month           = date('n', strtotime($order['order_date'])) - 1;
      $day             = date('j', strtotime($order['order_date']));
      $date            = 'new Date(' . $year . ', ' . $month . ', ' . $day . ')';
      $formatted_total = '$' . $order['grand_total'];
      $all_rows       .= '[{v: ' . $date . ', f: \'' . $formatted_date . '\'}, {v: ' . $order['grand_total'] . ', f: \'' . $formatted_total . '\'}],';
    }
    return rtrim($all_rows, ',');
  }

  /**
  * Recipient name formatted 'first_name last_name'
  *
  * @access private
  * @param int recipient_id
  * @return string Recipient's name formatted 'first_name last_name'
  */
  private function get_recipient_name($recipient_id) {
    $recipient = parent::recipient($recipient_id);
    return $recipient['first_name'] . ' ' . $recipient['last_name'];
  }
}

?>
