<?php
/**
  * Queries needed across classes
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-05-01
  */

class query {
  private $db;

  public function __construct() {
    $db       = new database;
    $this->db = $db;
  }

  public function __destruct() {
    $this->db = null;
  }

  /**
    * Returns array of all welfare recipients in the system
    *
    * @access protected
    * @param null
    * @return array All recipients in the system
    */
  protected function all_recipients() {
    $this->db->query('SELECT id, first_name, last_name FROM recipients ORDER BY last_name, first_name');
    return $this->db->resultset();
  }

  /**
    * Returns array about a single recipient (id, first_name, last_name)
    *
    * @access protected
    * @param int recipient_id
    * @return array Single recipient data (id, first_name, last_name)
    */
  protected function recipient($recipient_id) {
    $this->db->query('SELECT id, first_name, last_name FROM recipients WHERE id = :recipient_id');
    $this->db->bind(':recipient_id', $recipient_id);
    return $this->db->single();
  }

  /**
    * Returns oldest order date
    *
    * @access protected
    * @param null
    * @return string Oldest order date
    */
  protected function oldest_order_date() {
    $this->db->query('SELECT MIN(order_date) AS oldest_order_date FROM orders');
    return $this->db->single()['oldest_order_date'];
  }

  /**
    * Returns newest order date
    *
    * @access protected
    * @param null
    * @return string Newest order date
    */
  protected function newest_order_date() {
    $this->db->query('SELECT MAX(order_date) AS newest_order_date FROM orders');
    return $this->db->single()['newest_order_date'];
  }

  /**
    * Returns next item_id for use in calx javascript
    *
    * @access protected
    * @param null
    * @return int Next item_id that will be used
    */
  protected function next_item_id() {
    $this->db->query('SELECT MAX(id) + 1 AS next_item_id FROM items');
    return (int) $this->db->single()['next_item_id'];
  }

  /**
    * Returns form_id for currently active form
    *
    * @access protected
    * @param null
    * @return int form_id of currently active form
    */
  protected function active_form_id() {
    $this->db->query('SELECT id FROM forms WHERE active = "1"');
    return (int) $this->db->single()['id'];
  }

  /**
    * Returns form_id for specific order
    *
    * @access protected
    * @param int order_id
    * @return int form_id associated with the order_id
    */
  protected function form_id($order_id) {
    $this->db->query('SELECT form_id FROM orders WHERE id = :order_id');
    $this->db->bind(':order_id', $order_id);
    return (int) $this->db->single()['form_id'];
  }
}
