<?php
/**
  * Class to view past orders
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-01
  * @since 2014-04-29
  */

class past_orders extends query {
  /**
  * Returns array for use in template to view past orders
  *
  * @access public
  * @param null
  * @return array Data to be used in template
  */
  public function view() {
    $all_orders = $this->get_all_orders();
    return array('recipients' => parent::all_recipients(), 'chart' => $all_orders);
  }

  /**
  * Returns array of all orders in the system
  *
  * @access public
  * @param null
  * @return array All orders in the system
  */
  private function get_all_orders() {
    $db = new database;
    $db->query('SELECT o.id, o.order_date, r.id AS recipient_id, r.first_name, r.last_name, SUM(oi.quantity * i.unit_cost) AS grand_total FROM orders o LEFT JOIN orders_items oi ON o.id = oi.order_id LEFT JOIN items i ON oi.item_id = i.id LEFT JOIN recipients r ON o.recipient_id = r.id GROUP BY o.id ORDER BY o.order_date DESC');
    return $db->resultset();
  }
}

?>
