<?php
/**
  * Class to view statistics about past orders
  *
  * @author  Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-03
  * @since 2014-04-29
  */

class statistics extends query {

  /**
  * Returns array for use in template to view past orders
  *
  * @access public
  * @param null
  * @return array Data to be used in template
  */
  public function view() {
    $annual_summary              = $this->get_annual_summary();
    $annual_summary_by_recipient = array('rows' => $this->get_annual_summary_by_recipient(), 'selected_year' => parent::newest_order_date());
    $monthly_summary             = array('recipients' => parent::all_recipients(), 'rows' => $this->get_monthly_summary(), 'gridline_count' => $this->get_monthly_summary_gridline_count());
    return array('annual_summary' => $annual_summary, 'annual_summary_by_recipient' => $annual_summary_by_recipient, 'monthly_summary' => $monthly_summary);
  }

  /**
  * Gets array with information about all orders for each year in the system
  *
  * @access private
  * @param null
  * @return array Yearly averages of order data
  */
  private function get_annual_summary() {
    $db = new database;
    $db->query('SELECT YEAR(o.order_date) AS year, SUM(oi.quantity * i.unit_cost) AS grand_total, COUNT(DISTINCT(o.id)) AS num_orders, ROUND((SUM(oi.quantity * i.unit_cost) / COUNT(DISTINCT(o.id))), 2) AS average FROM orders o LEFT JOIN orders_items oi ON o.id = oi.order_id LEFT JOIN items i ON oi.item_id = i.id GROUP BY year DESC WITH ROLLUP');
    return $db->resultset();
  }

  /**
  * Gets annual summary data by recipient from database
  *
  * @access private
  * @param null
  * @return array Annual summary data by recipient
  */
  private function get_annual_summary_by_recipient() {
    $db = new database;
    $db->query('SELECT r.id, r.first_name, r.last_name, SUM(oi.quantity * i.unit_cost) AS total_cost, COUNT(DISTINCT(o.id)) AS num_orders, ROUND((SUM(oi.quantity * i.unit_cost) / COUNT(DISTINCT(o.id))), 2) AS average, YEAR(o.order_date) AS year FROM orders o LEFT JOIN recipients r ON o.recipient_id = r.id LEFT JOIN orders_items oi ON o.id = oi.order_id LEFT JOIN items i ON oi.item_id = i.id GROUP BY year DESC, o.recipient_id ORDER BY year DESC, r.last_name ASC');
    return $db->resultset();
  }

  /**
  * Gets monthly summary data from database
  *
  * @access private
  * @param null
  * @return array Monthly summary data from database
  */
  private function get_monthly_summary() {
    $monthly_summary_sql = $this->get_monthly_summary_sql();
    $db = new database;
    $db->query('SELECT o.order_date' . $monthly_summary_sql . ' FROM orders o LEFT JOIN orders_items oi ON o.id = oi.order_id LEFT JOIN items i ON oi.item_id = i.id GROUP BY YEAR(o.order_date), MONTH(o.order_date) ORDER BY o.order_date DESC');
    return $db->resultset();
  }

  /**
  * Generates SQL needed to get monthly summary data for each recipient by month
  *
  * @access private
  * @param null
  * @return string SQL to be used in query to get monthly summary data by recipient
  */
  private function get_monthly_summary_sql() {
    $monthly_summary_sql = null;
    foreach(parent::all_recipients() AS $recipient) {
      $id = $recipient['id'];
      // http://dba.stackexchange.com/questions/28406/group-by-two-columns
      $monthly_summary_sql .= ', SUM(CASE WHEN o.recipient_id = ' . $id . ' THEN oi.quantity * i.unit_cost ELSE 0 END) AS r' . $id;
    }
    return $monthly_summary_sql;
  }

  /**
  * Monthly summary data formatted for
  * Google Visualization API javascript to parse
  *
  * @access private
  * @param array Monthly summary by recipient
  * @return string Monthly summary by recipient data formatted for Google Visualization API
  */
  private function get_monthly_summary_gridline_count() {
    // Find number of months between first entry in database and last entry.
    // Formula is not inclusive so must add 1 to get the number of months.
    // Need to add one more so that the max number of gridlines is one more
    // than there are months in the database.
    $oldest_order_date = new DateTime(parent::oldest_order_date());
    $newest_order_date = new DateTime(parent::newest_order_date());
    return $oldest_order_date->diff($newest_order_date)->m + ($oldest_order_date->diff($newest_order_date)->y*12) + 2;
  }
}

?>
