<?php
/**
  * Summary data for past orders
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-03-12
  * @since 2014-03-12
  *
  */

require_once 'config.php';

$recipient = (int) $_REQUEST['recipient'];

if($recipient == '') {
  $html = array('html' => 'Please go back and select a recipient to view.');
  template::display('generic.tmpl', array('html' => $html, 'title' => 'Individual Summary'));
} else {
  $summary = new summary;
  template::display('summary.tmpl', $summary->view($recipient));
}

?>
