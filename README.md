# LDS Church Welfare Tracking Database

## Introduction
This database assists ward leaders track patterns in welfare food order assistance. The included data estimates cost for items available from the Bishop’s Storehouse form.

## Installation
1. Download the files and place them in an accessible directory on your web server
2. Create a database to be used by the application
3. Run the SQL commands found in `sql.sql`
4. Rename `config.example.php` to `config.php`
5. Update `config.php` to include information to access your database and the full, web-accessible, URL of the application
6. This application does not include any kind of authentication—it is suggested that you password protect the application directory to limit access to only those that really need it

## Known Issues
1. There is not currently a way to delete a user using the web interface
2. There is no easy way to add another form (needed when the Bishop’s Storehouse form changes)
3. It has only been tested using the latest Chrome on Mac OS X 10.9 but seems to work well in other browsers on other systems

**This project is not affiliated with, nor endorsed by, the [LDS Church].**

[LDS Church]: http://www.lds.org
