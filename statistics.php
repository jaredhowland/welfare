<?php
/**
  * Statistics page
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-04-29
  * @since 2014-04-29
  *
  */

require_once 'config.php';

$statistics = new statistics;
$stats_data = $statistics->view();

template::display('statistics.tmpl', array('title' => 'Statistics', 'annual_summary' => $stats_data['annual_summary'], 'annual_summary_by_recipient' => $stats_data['annual_summary_by_recipient'], 'monthly_summary' => $stats_data['monthly_summary']));

?>
