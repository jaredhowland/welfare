<?php
/**
  * Update order table data
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-05
  * @since 2014-05-03
  *
  */

require_once 'config.php';

$id    = $_REQUEST['id'];
$value = trim($_REQUEST['value']);

$type = explode('_', $id);
$id   = (int) $type[1];
$type = trim($type[0]); // item or unitcost

if($type == 'item') {
  if(update_item($value, $id)) {
    echo get_item($id);
  } else {
    echo '<span class="label label-danger">Error</span>';
  }
} elseif($type == 'unitcost'){
  if(update_unit_cost($value, $id)) {
    echo get_unit_cost($id);
  } else {
    echo '<span class="label label-danger">Error</span>';
  }
}

function update_item($item, $id) {
  $item = trim($item);
  $db = new database;
  $db->query('UPDATE items SET item = :item WHERE id = :id');
  $db->bindMore(array(':item' => $item, ':id' => $id));
  return $db->execute();
}

function update_unit_cost($unit_cost, $id) {
  if(is_numeric($unit_cost)) {
    $db = new database;
    $db->query('UPDATE items SET unit_cost = :unit_cost WHERE id = :id');
    $db->bindMore(array(':unit_cost' => $unit_cost, ':id' => $id));
    return $db->execute();
  } else {
    return false;
  }
}

function get_item($id) {
  $db = new database;
  $db->query('SELECT item FROM items WHERE id = :id');
  $db->bind(':id', $id);
  return $db->single()['item'];
}

function get_unit_cost($id) {
  $db = new database;
  $db->query('SELECT unit_cost FROM items WHERE id = :id');
  $db->bind(':id', $id);
  return $db->single()['unit_cost'];
}

?>
