<?php
/**
  * Update an existing welfare order
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-04-29
  * @since 2014-03-13
  *
  */

require_once 'config.php';

$update = new update;
$html = $update->order($_POST);

template::display('generic.tmpl', array('html' => array('html' => $html)));

?>
