<?php
/**
  * Update order table data
  *
  * @author Jared Howland <welfare@jaredhowland.com>
  * @version 2014-05-05
  * @since 2014-05-05
  *
  */

require_once 'config.php';

$id = $_REQUEST['id'];

$type = explode('_', $id);
$id   = (int) $type[1];
$type = $type[0]; // item or unitcost

if($type == 'item') {
  echo get_item($id);
} elseif($type == 'unitcost') {
  echo get_unit_cost($id);
}

function get_item($id) {
  $db = new database;
  $db->query('SELECT item FROM items WHERE id = :id');
  $db->bind(':id', $id);
  return $db->single()['item'];
}

function get_unit_cost($id) {
  $db = new database;
  $db->query('SELECT unit_cost FROM items WHERE id = :id');
  $db->bind(':id', $id);
  return $db->single()['unit_cost'];
}

?>
